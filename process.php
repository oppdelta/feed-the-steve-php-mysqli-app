<?php
include("include/session.php");

class Process
{
    function __construct(){
      global $session;
    
        if(isset($_POST['subNewBase'])){
         $this->procNewBase();
        }
        /* User submitted edit account form */
        else if(isset($_POST['subEdit'])){
           $this->procEditAccount();
        }
        else if(isset($_POST['subLogin'])){
           $this->procLogin();
        }
        else if(isset($_POST['subNewInviteCode'])){
           $this->procNewInvite();
        }
        else if(isset($_POST['subNewAlert'])){
         $this->procNewAlert();
        }
        else if($session->logged_in){
         $this->procLogout();
        }
        else{
          header("Location: index.php");
       }
    }
    
    /* PROCESSES BELOW */
    
function procNewInvite() {
    global $database,$session;
    
    $_SESSION['invitecode'] = $database->generateInviteCode();
    header("Location: ".$session->referrer);
}
    
   function procNewBase() {
        global $session, $form, $database;
        
        if($session->logged_in) {
                  
            $form->setValue("coordX",$_POST['coordX']);
            $form->setValue("coordY",$_POST['coordY']);
            $form->setValue("coordZ",$_POST['coordZ']);
            $form->setValue("baseType",$_POST['baseType']);

            
            $retval = $session->sessionAddNewBase($session->username, $_POST['coordX'],$_POST['coordY'],$_POST['coordZ'],$_POST['baseType']);

            /* Adding Successful */
            if($retval == 0){
                
                $_SESSION['alert'] = "Base has been added!";
                header("Location: ".$session->referrer);
            }
            /* Error found with form */
            else if($retval == 1){
                $_SESSION['alert'] = "FAILED To add base";
                $_SESSION['value_array'] = $_POST;
                $_SESSION['error_array'] = $form->getErrorArray();
                header("Location: ".$session->referrer);
            }
            /* Adding attempt failed */
            else if($retval == 2){
                $_SESSION['alert'] = "FAILED To add base";
                $_SESSION['baseadd'] = false;
                header("Location: ".$session->referrer);
            }
        }
        else {
            $_SESSION['alert'] = "Need to be logged in to post a new base!";
            header("Location: ".$session->referrer);
        }
   }
    
   function procNewALert() {
       global $session, $form, $database;
   }
   
   function procLogin(){
      global $session, $form;
      /* Login attempt */
      
      
      $retval = $session->login($_POST['loginusername'],$_POST['loginpassword'],$_POST['loginremember']);
      
      /* Login successful */
      if($retval){
          $_SESSION['help'] = "You logged in wooo <br/>";
            header("Location: ".$session->referrer);
          //echo $session->referrer;
      }
      /* Login failed */
      else{
          $_SESSION['help'] = "Form error";
         $_SESSION['value_array'] = $_POST;
         $_SESSION['error_array'] = $form->getErrorArray();
         header("Location: ".$session->referrer);
         
      }
   }

   function procLogout(){
      global $session;
      $retval = $session->logout();
      header("Location: register.php");
   }
   function procRegister(){
      global $session, $form;
      /* Convert username to all lowercase (by option) */
      if(ALL_LOWERCASE){
         $_POST['username'] = strtolower($_POST['username']);
      }
      
      $form->setValue("username",$_POST['username']);
      $form->setValue("password",$_POST['password']);
      $form->setValue("password2",$_POST['password2']);
      $form->setValue("email",$_POST['email']);
      $form->setValue("email2",$_POST['email2']);
      $form->setValue("displayName",$_POST['displayName']);
      
      
      
      /* Registration attempt $subuser, $subpass, $subpass2, $subemail, $email2, $displayName, $steamName, $skypeName*/
      $retval = $session->register($_POST['username'], $_POST['password'], $_POST['password2'], $_POST['email'], $_POST['email2'], $_POST['displayName'], $_POST['steamName'], $_POST['skypeName'], $_POST['referedBy']);
      
      /*
      echo $_POST['username'];
      echo $_POST['password'];
      echo $_POST['password2'];
      echo $_POST['email'];
      echo $_POST['email2'];
      echo $_POST['displayName'];
      echo $_POST['steamName'];
      echo $_POST['skypeName'];*/
      
      /* Registration Successful */
      if($retval == 0){
         $_SESSION['reguname'] = $_POST['username'];
         $_SESSION['regsuccess'] = true;
         header("Location: ".$session->referrer);
      }
      /* Error found with form */
      else if($retval == 1){
         $_SESSION['value_array'] = $_POST;
         $_SESSION['error_array'] = $form->getErrorArray();
         header("Location: ".$session->referrer);
      }
      /* Registration attempt failed */
      else if($retval == 2){
         $_SESSION['reguname'] = $_POST['username'];
         $_SESSION['regsuccess'] = false;
         header("Location: ".$session->referrer);
      }
       
       
   }
   
   function procResetPassword(){
      global $session, $form;
      /* Convert username to all lowercase (by option) */
      if(ALL_LOWERCASE){
         $_POST['username'] = strtolower($_POST['username']);
      }
      
      $form->setValue("username",$_POST['username']);
      $form->setValue("password",$_POST['password']);
      $form->setValue("password2",$_POST['password2']);
      $form->setValue("email",$_POST['email']);
      
      
      /* Reset attempt */
      $retval = $session->resetPassword($_POST['username'], $_POST['password'], $_POST['password2'], $_POST['email']);

      /* Registration Successful */
      if($retval == 0){
         $_SESSION['resetuname'] = $_POST['username'];
         $_SESSION['resetsuccess'] = true;
         header("Location: ".$session->referrer);
      }
      /* Error found with form */
      else if($retval == 1){
         $_SESSION['value_array'] = $_POST;
         $_SESSION['error_array'] = $form->getErrorArray();
         header("Location: ".$session->referrer);
      }
      /* Registration attempt failed */
      else if($retval == 2){
         $_SESSION['resetuname'] = $_POST['username'];
         $_SESSION['resetsuccess'] = false;
         header("Location: ".$session->referrer);
      }
       
       
   }
};

$process = new Process;
?>
