<div class="row">
    <div class="col-md-6">
        <div class="well well-sm">
            <form class="form-inline" role="form" action='process.php' method='POST'>
                <h4>Invite Codes</h4>
                <button type="submit" class="btn btn-default">Generate Invite Code</button>
                <input type="email" class="form-control" placeholder="<?php if(isset($_SESSION['invitecode'])) {
                                                                                    echo $_SESSION['invitecode']; 
                                                                                    unset($_SESSION['invitecode']);

                } ?>">
                <input type="hidden" value="1" name="subNewInviteCode" />
            </form>
        </div>
    </div>
    <div class="col-md-6">
        <div class="well well-sm">
            <h4>Your invite codes</h4>
            <table class="table table-striped table-condensed" style="font-size: 1em;">
                <?php $database->displayInviteCodes($session->username);?>
            </table>
        </div>
    </div>
</div>