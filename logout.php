<?php
include("include/session.php");

global $session;
      /**
       * Delete cookies - the time must be in the past,
       * so just negate what you added when creating the
       * cookie.
       */
      if(isset($_COOKIE['cookname']) && isset($_COOKIE['cookid'])){
         setcookie("cookname", "", time()-COOKIE_EXPIRE, COOKIE_PATH);
         setcookie("cookid",   "", time()-COOKIE_EXPIRE, COOKIE_PATH);
      }

      /* Unset PHP session variables */
      unset($_SESSION['username']);
      unset($_SESSION['userid']);

      /* Reflect fact that user has logged out */
      $session->logged_in = false;
      
      /**
       * Remove from active users table and add to
       * active guests tables.
       */
      /*
      $database->removeActiveUser($this->username);
      $database->addActiveGuest($_SERVER['REMOTE_ADDR'], $this->time);*/
      
      /* Set user level to guest */
      $session->username  = GUEST_NAME;
      $session->userlevel = GUEST_LEVEL;
      
      header('Location: index.php');
exit;
?>
