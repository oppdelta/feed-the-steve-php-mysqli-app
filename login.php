<?php
include("include/session.php");
global $session;
global $database;
global $form;

$session->page = "login";

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Feed the Steve - Login</title>

        <!-- Bootstrap -->
        <style>
        @import url('css/bootstrap.min.css');
        @import url('css/style.css');
        </style>
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>

        <script type="text/javascript">
            $(function() {
                $(".tip").tooltip();
            })
        </script>
    </head>
    <body>
        <?php 
        //Include the navbar
        include_once 'module/nav.php';

        ?>
        <!--Whitelist List -->
      <div class="container">

          <?php  

          if($session->logged_in) { echo "Already logged in, redirecting...";
            header('Location: index.php');
          }

          ?>

        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <h2>Login</h2>
                <div class="well well-sm">
                    <p>Usernames are case sensitive. Use your exact Minecraft one.</p>
                    <form class="form-horizontal" role="form" action="process.php" method="POST">
                        <div class="form-group">
                          <label for="inputUsername3" class="col-sm-2 control-label">Username</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputUsername3" name="loginusername" placeholder="Username" value="oppdelta">
                            <?php echo $form->error("loginuser"); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                          <div class="col-sm-10">
                            <input type="password" class="form-control" id="inputPassword3" name="loginpassword" placeholder="Password" value="b1c14xR">
                            <?php echo $form->error("loginpass"); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" name="loginremember" checked="1"> Remember me

                                <input type="hidden" name="subLogin" value="1">
                              </label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Login</button>
                          </div>
                        </div>
                      </form>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>

      </div>
    </body>
</html>

