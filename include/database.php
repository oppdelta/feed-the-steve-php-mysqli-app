<?php
include("constants.php");
      
class MySQLDB
{
    var $conn;         //The MySQL database connection
    var $num_members;        //Number of signed-up users
    var $userinfo = array();  //The array holding all user info
   
   /* Class constructor */
   function __construct(){
      /* Make connection to database */
      $this->conn = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
      if (mysqli_connect_errno())
       die(mysqli_connect_error());
      
      $this->num_members = $this->getTotalUsers();
   }
   
   function addNewBase($subuser, $subx, $suby, $subz, $subtype) {
          
        $subuser = mysqli_real_escape_string($this->conn,$subuser);
        $subx = mysqli_real_escape_string($this->conn,$subx);
        $suby = mysqli_real_escape_string($this->conn,$suby);
        $subz = mysqli_real_escape_string($this->conn,$subz);
        $subtype = mysqli_real_escape_string($this->conn,$subtype);

        $userID = $this->getUserID($subuser);
        $coords = "$subx,$suby,$subz";
        //"UPDATE users SET ".$field." = '".$value."' WHERE username = '".$username."';
       
        if($subtype == "1") {
            
            if($this->hasHQ($userID)){
                
                $query = $this->conn->prepare("UPDATE bases SET coordinates = ? WHERE base_type = 1 AND user_id = ?;");
                $query->bind_param("si", $coords,$userID);
            }
            else {
                $query = $this->conn->prepare("INSERT INTO bases (user_id, coordinates, base_type) VALUES (?, ?, ?);");
                $query->bind_param("isi", $userID, $coords, $subtype);
            }
        }
        else {
                $query = $this->conn->prepare("INSERT INTO bases (user_id, coordinates, base_type) VALUES (?, ?, ?);");
                $query->bind_param("isi", $userID, $coords, $subtype);
            }
        
        
        
        /* Bind parameters
         s - string, b - blob, i - int, etc */
        
         
        return $query->execute();
   }
   
   function generateInviteCode() { 
        global $session;
       
        $code = md5($session->generateRandStr(18));
        $code = mb_substr($code, 0, 6);
       
        $userid = $this->getUserID($session->username);
       
        $query = $this->conn->query("INSERT INTO invite_codes (invite_id, user_id, code, gen_date, used) VALUES (NULL, '$userid', '$code', CURRENT_TIMESTAMP, '0');");
       
        
        return $code;
   }
   
   function displayInviteCodes($subuser) {
        global $session;
       
        if (!isset($subuser)) {
            $subuser = $session->username;
            
        }
        
        $userid = $this->getUserID($subuser);
        
        $query = $this->conn->query("SELECT code, used FROM invite_codes WHERE user_id = $userid AND used = 0 ORDER BY invite_id DESC LIMIT 4");
  

        echo '<th class="col-md-4">Code</th>
             <th class="col-md-4">Status</th>';
       
        while($result = $query->fetch_assoc())
        {
            echo'
                <tr>
                    <td class="col-md-4">
                        '.$result['code'].'
                    </td>
                    <td class="col-md-4">
                        '.$result['used'].'
                    </td>
                </tr>';
        }
   }
   
   
   function hasHQ($subid) {
        $subid = mysqli_real_escape_string($this->conn,$subid);
        
        /*$query = $this->conn->prepare("SELECT * FROM `bases` WHERE user_id = ? AND base_type = 0");
        $query->bind_param("i", $subid);
        
        $query->execute();*/

   
        
        $query = $this->conn->prepare("SELECT * FROM bases WHERE user_id = ? AND base_type = 1 LIMIT 1");
        $query->bind_param("i", $subid);
        $query->execute();
        $query->store_result();

        $rows = $query->num_rows;
        
        if($rows > 0)
            return true;
        else
            return false;
        
   }
   
   
   /**
    * addNewUser - Inserts the given (username, password, email)
    * info into the database. Appropriate user level is set.
    * Returns true on success, false otherwise.
    */                
   function addNewUser($subuser, $subpass, $subemail, $displayName, $steamName, $skypeName,$code,$referedBy) {
          
        $subuser = mysql_real_escape_string($subuser);
        $subpass = mysql_real_escape_string($subpass);
        $subemail = mysql_real_escape_string($subemail);
        $displayName = mysql_real_escape_string($displayName);
        $steamName = mysql_real_escape_string($steamName);
        $code = mysql_real_escape_string($code);
        $skypeName = mysql_real_escape_string($skypeName);
        $referedBy = mysql_real_escape_string($referedBy);
        
        
      $time = time();
      /* If admin sign up, give admin user level */
      if(strcasecmp($subuser, ADMIN_NAME) == 0){
         $ulevel = ADMIN_LEVEL;
         
         
      }else{
         $ulevel = USER_LEVEL;
      }
      

          $q = "INSERT INTO ".TBL_USERS."(username, password, email, display_name, steam_name, skype_name,activationcode,referedBy) VALUES ('$subuser', 'temp', '$subemail', '$displayName', '$steamName', '$skypeName','$code','$referedBy')";
      
          
          return mysql_query($q, $this->connection);
  
   }
   
   function nCrypt($password,$username) {
        $userinfo = $this->getUserInfo($username);
       
        $password = mysql_real_escape_string($password);
        $passwordmd5 = md5(strtoupper($username).SEED.$password);
        $password = $passwordmd5.sha1(SEED.$password.$username);
        
        $password = $passwordmd5.sha1($password.strlen($password));
        
        $password = hash("sha256",$userinfo[4].$password);
        
        return $password;
   }
   
   function getTotalUsers() {
        $query = $this->conn->query("SELECT COUNT(User_Id) FROM users");
        $result = $query->fetch_row();
        $query->close();

        return $result[0];
   }
   
   function latestAlert() {
        $query = $this->conn->query("SELECT alerts.text, users.username "
                . "FROM alerts "
                . "INNER JOIN users ON alerts.user_id=users.user_id "
                . "ORDER BY alerts.alert_id DESC "
                . "LIMIT 1;");
        $result = $query->fetch_assoc();
        $query->close();

        return $result;
   }
   
   
   function displayHomeBases() {
       $from = 0;
       
       
        if (isset($_GET['from'])) {
            $from = mysqli_real_escape_string($this->conn, $_GET['from']);
        }
        if($from < 0)
            $from = 0;
        $query = $this->conn->query("SELECT users.username, bases.coordinates FROM users INNER JOIN bases ON users.user_id = bases.user_id AND bases.base_type = 1 LIMIT $from,10");
        
        $prev = $from - 10;
        $next = $from + 10;
        $current = $from / 10 + 1;
        
        
        
        echo '
        <tr> 
        <ul class="pager">
          <li><a href="'.$_SERVER["PHP_SELF"].'?from='.$prev.'">&laquo; Prev</a></li>
            <li class="disabled"><a>'.$current.'</a></li>
          <li><a href="'.$_SERVER["PHP_SELF"].'?from='.$next.'"> Next &raquo;</a></li>
        </ul>
        </tr>
        ';
        
        echo '<th class="col-md-3">Player</th>
             <th class="col-md-6">Headquarters</th>';
       
        while($result = $query->fetch_assoc())
        {
            echo'
                <tr>
                    <td class="col-md-3">
                        <img src="http://minecraft.aggenkeech.com/face.php?u='.$result['username'].'&s=32&d" alt="'.$result['username'].
                    ' face" data-toggle="tooltip" data-placement="right" title="'.$result['username'].'" class="tip img-thumbnail"/>
                    </td>
                    <td class="col-md-9">
                        '.$result['coordinates'].'
                    </td>
                </tr>';
        }
        
        

   }
   
   //Returns boolean
   function confirmUserPass($subuser,$subpassword){
      /* Add slashes if necessary (for query) */
      if(!get_magic_quotes_gpc()) {
	      $subuser = addslashes($subuser);
      }
      
      
      
        $subuser = mysqli_real_escape_string($this->conn,$subuser);
        $subpassword = mysqli_real_escape_string($this->conn,$subpassword);

        
        $query = $this->conn->query("SELECT password FROM users WHERE username='$subuser' LIMIT 1;");
        $result = $query->fetch_row();
        $query->close();

        $result[0] = stripslashes($result[0]);
        
        //Return username failure
        
        //Password check
        $password = $this->nCrypt($subpassword,$subuser);
        
        
        /* Validate that password is correct */
        if($password == $result[0]){
            $_SESSION['help2'] = "Password correct";
           return 0; //Success! Username and password confirmed
        }
        else{
            $_SESSION['help2'] = "<br />Returned password: ".$password.
                    "<br/><br>Username used: ".$subuser.
                    "<br>Password sent: ".$subpassword;
           return 2;
        }
      
   }
   
   /**
    * confirmUserID - Checks whether or not the given
    * username is in the database, if so it checks if the
    * given userid is the same userid in the database
    * for that user. If the user doesn't exist or if the
    * userids don't match up, it returns an error code
    * (1 or 2). On success it returns 0.
    */
   function confirmUserID($subuser, $loginid){
       
        $subuser = mysqli_real_escape_string($this->conn,$subuser);
        $loginid = mysqli_real_escape_string($this->conn,$loginid);
        
        
        
        if(!get_magic_quotes_gpc()) {
	      $subuser = addslashes($subuser);
        }
        
        $query = $this->conn->query("SELECT loginid FROM users WHERE username='$subuser' LIMIT 1;");
        $result = $query->fetch_row();
        $query->close();

        $result[0] = stripslashes($result[0]);

        
            
        /* Validate that userid is correct */
        if($loginid == $result[0]){
           return 0; //Success! Username and userid confirmed
        }
        else{
           return 2; //Indicates userid invalid
        }
   }
   
   /**
    * usernameTaken - Returns true if the username has
    * been taken by another user, false otherwise.
   */
   function usernameTaken($username){
       $username = mysql_real_escape_string($username);
      if(!get_magic_quotes_gpc()){
         $username = addslashes($username);
      }
      $q = "SELECT username FROM ".TBL_USERS." WHERE username = '$username' LIMIT 1";
      $result = mysql_query($q, $this->conn);
      return (mysql_numrows($result) > 0);
   }
   
   function getUserInfo($subuser){
        $subuser = mysqli_real_escape_string($this->conn,$subuser);
        
        $query = $this->conn->query("SELECT username,password,level,loginid,date_registered FROM users WHERE username='$subuser' LIMIT 1;");
        $result = $query->fetch_row();
        $query->close();

        return $result;
   }
   
   function getUserID($username){
      $username = mysqli_real_escape_string($this->conn,$username);

        $query = $this->conn->prepare("SELECT user_id FROM users WHERE username = ? LIMIT 1");

        /* Bind parameters
         s - string, b - blob, i - int, etc */
        $query -> bind_param("s", $username);

        /* Execute it */
        $query -> execute();

        /* Bind results */
        $query -> bind_result($result);

        /* Fetch the value */
        $query -> fetch();

      return $result;
   }
   
   
    function getAllEmails(){
     
      $q = "SELECT id,username,password,email 
            FROM  `opp_user` LIMIT 10";
      $result = mysql_query($q, $this->connection);
      
      return $result;
   }
   
   
   function query($q){
      return mysql_query($q, $this->connection);
   }
 
   /**
    * updateUserField - Updates a field, specified by the field
    * parameter, in the user's row of the database.
    */
   function updateUserField($username, $field, $value){

       $username = mysqli_real_escape_string($this->conn,$username);
       $field = mysqli_real_escape_string($this->conn,$field);
       $value = mysqli_real_escape_string($this->conn,$value);
       
        //$query = $this->conn->prepare("UPDATE users SET '?' = ? WHERE username = ?");
        $query = $this->conn->query("UPDATE users SET ".$field." = '".$value."' WHERE username = '".$username."';");
        /* Bind parameters
         s - string, b - blob, i - int, etc */
        //$query->bind_param('sss',$field,$value,$username);

      return $query;
   }
   

   function emailALL() {
      
	
    		$to = "nathan.la.harvey@gmail.com";
			$subject = "OPPCraft - TEST EMAIL";
			$body = "Good day everyone at OPPCraft.\r\n";
			$headers= "From: OPPCraft.net";

     			mail($to, $subject, $body, $headers);
    		
	

		
	/*
			$to = "bossyhar14@hotmail.co.uk";
			$subject = "OPPCraft - Server Reunion";
			$body = "Good day everyone at OPPCraft.\r\n
			
			With the new release of 1.5 the staff would like to offer an invitation for a little reunion on the server this Saturday. Come on by, if you can, around the times 6 - 10 PM (GMT). 2 -7PM(Est). Although the activities can carry on for longer after these times.
			\r\n
			With the new redstone changes I'd like to suggest a redstone related competition. We would like to get a few games running.
			\r\n
			- Classic spleef.\n
			- Build competitions.\n
			- PVP.\n
			- Lotteries.\n
			\r\n
			All with prizes to be won. A few up for grabs are: Free donator rank for the server, game of the winner's choosing, in server items and more.
			\r\n
			If you need more details feel free to reply to this message or comment on the blog post.
			\r\n
			Thank you for your time. \n
			~OPPDelta (Owner). \n
			__________________________\n
			Server IP: play.oppcraft.net // s13.hosthorde.com:25566\n
			BLOG: http://blog.oppcraft.net";
			$headers = "From: OPPCraft Minecraft" . "\r\n" ;
			$headers .= 'Reply-To: nathan.la.harvey@gmail.com' . "\r\n";
			//$headers .= "BCC: bossyhar14@hotmail.co.uk";
			$headers .= "Bcc: '".$bcc."'\r\n";

     			mail($to, $subject, $body, $headers);*/
		
    

   }
};

$database = new MySQLDB;
?>