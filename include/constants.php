<?php
/* Database Constants */
define("DB_SERVER", "localhost");
define("DB_USER", "root" );
define("DB_PASS", "");
define("DB_NAME", "fts");


/* Guest Constants */
define("GUEST_LEVEL", "0");
define("GUEST_NAME", "Guest");
define("SEED","dfg$%$^&*JKH%^K£H%£2CACz1");


define("COOKIE_EXPIRE", 60*60*24*100);  //100 days by default
define("COOKIE_PATH", "/");  //Avaible in whole domain

?>