<?php
include("database.php");
include("form.php");

class Session
{
   var $username;     //Username given on sign-up
   var $userid;       //Random value generated on current login
   var $userlevel;    //The level to which the user pertains
   var $time;         //Time user was last active (page loaded)
   var $logged_in;    //True if user is logged in, false otherwise
   var $userinfo = array();  //The array holding all user info
   var $url;          //The page url current being viewed
   var $page = "";
   
   /* Class constructor */
   function __construct(){
      $this->time = time();
      $this->startSession();
   }

   function startSession(){
        global $database;  //The database connection
        session_start();   //Start the session
      
       /* Determine if user is logged in */
        $this->logged_in = $this->checkLogin();
      
        if(!$this->logged_in){
            $this->username = $_SESSION['username'] = GUEST_NAME;
            $this->userlevel = GUEST_LEVEL;
         }

         /* Set referrer page */
         if(isset($_SESSION['url'])){
            $this->referrer = $_SESSION['url'];
         }else{
            $this->referrer = "/";
         }

         /* Set current url */
         $this->url = $_SESSION['url'] = $_SERVER['PHP_SELF'];
   }
  
   
   function checkLogin(){
        global $database;  //The database connection
      
        if(isset($_COOKIE['cookname']) && isset($_COOKIE['cookid'])){
            $this->username = $_SESSION['username'] = $_COOKIE['cookname'];
            $this->userid   = $_SESSION['loginid']   = $_COOKIE['cookid'];
        }

        
       
        
      /* Username and userid have been set and not guest */
      if(isset($_SESSION['username']) && isset($_SESSION['loginid']) &&
         $_SESSION['username'] != GUEST_NAME){
          
            if($database->confirmUserID($_SESSION['username'], $_SESSION['loginid']) != 0){
            /* Variables are incorrect, user not logged in */
            unset($_SESSION['username']);
            unset($_SESSION['loginid']);
            return false;
         }
         
         //username,password,level,loginid,date_registered
         
         /* User is logged in, set class variables */
         $this->userinfo  = $database->getUserInfo($_SESSION['username']);
        // $this->username  = $this->userinfo['username'];
         $this->username  = $this->userinfo[0];
         $this->loginid    = $this->userinfo[3];
         $this->userlevel = $this->userinfo[2];
         return true;
      }
      /* User not logged in */
      else{
         return false;
      }
   }
   
   function login($subuser, $subpass, $subremember){
      global $database, $form;  //The database and form object

      
      $subpass = mysqli_real_escape_string($database->conn,$subpass);
      $subuser = mysqli_real_escape_string($database->conn,$subuser);
       $subremember = mysqli_real_escape_string($database->conn,$subremember);
      
      
      /* Username error checking */
      $field = "loginuser";  //Use field name for username
      if(!$subuser || strlen($subuser = trim($subuser)) < 3){
         $form->setError($field, "* Username not entered");
      }
      

      /* Password error checking */
      $field = "loginpass";  //Use field name for password
      if(!$subpass){
         $form->setError($field, "* Password not entered");
      }
      
      /* Return if form errors exist */
      if($form->num_errors > 0){
         return false;
      }
      
      /* Checks that username is in database and password is correct */
      $subuser = stripslashes($subuser);
      $result = $database->confirmUserPass($subuser,$subpass);

      /* Check error codes */
      if($result == 1){
         $field = "loginuser";
         $form->setError($field, "* Username not found");
      }
      else if($result == 2){
         $field = "loginpass";
         $form->setError($field, "* Invalid password");
      }
      
      /* Is the user banned? */
      /*
      if($database->usernameBanned($subuser))
            $form->setError($field, "* Username banned");
      */
      
      
      /* Return if form errors exist */
      if($form->num_errors > 0){
          
         return false;
      }
//username,password,level,loginid,date_registered
      /* Username and password correct, register session variables */
      $this->userinfo  = $database->getUserInfo($subuser);
      $this->username  = $_SESSION['username'] = $this->userinfo[0];
      $this->loginid    = $_SESSION['loginid']   = $this->generateRandID();
      $this->userlevel = $this->userinfo[3];
     
      /* Insert userid into database and update active users table */
      $q = $database->updateUserField($this->username, "loginid", $this->loginid);

       $_SESSION['help3'] = "<br>Sending user: ".$this->username.
                            "<br>Sending loginid: ".$this->loginid.
                            "<br>Query result: ".$q.
                            "<br>";

      /**
       * This is the cool part: the user has requested that we remember that
       * he's logged in, so we set two cookies. One to hold his username,
       * and one to hold his random value userid. It expires by the time
       * specified in constants.php. Now, next time he comes to our site, we will
       * log him in automatically, but only if he didn't log out before he left.
       */
      
      
      if($subremember){
         setcookie("cookname", $this->username, time()+COOKIE_EXPIRE, COOKIE_PATH);
         setcookie("cookid",   $this->loginid,   time()+COOKIE_EXPIRE, COOKIE_PATH);
      }

      /* Login completed successfully */
      return true;
   }
   
   
   
   function logout(){
      global $database;  //The database connection
      /**
       * Delete cookies - the time must be in the past,
       * so just negate what you added when creating the
       * cookie.
       */
      if(isset($_COOKIE['cookname']) && isset($_COOKIE['cookid'])){
         setcookie("cookname", "", time()-COOKIE_EXPIRE, COOKIE_PATH);
         setcookie("cookid",   "", time()-COOKIE_EXPIRE, COOKIE_PATH);
      }

      /* Unset PHP session variables */
      unset($_SESSION['username']);
      unset($_SESSION['loginid']);

      /* Reflect fact that user has logged out */
      $this->logged_in = false;
      
      /**
       * Remove from active users table and add to
       * active guests tables.
       */
      
      /* Set user level to guest */
      $this->username  = GUEST_NAME;
      $this->userlevel = GUEST_LEVEL;
   }
   
   //register($_POST['username'], $_POST['password'], $_POST['password2'], $_POST['email'], $_POST['email2'], $_POST['displayName'], $_POST['steamName'], $_POST['skypeName']);
      
function sessionAddNewBase($subuser, $subx, $suby, $subz, $subtype) {
            global $database, $form;
            //If the username is not manually set, then use the session username.
            if(!isset($subuser))
                $subuser = $this->username;
            
            //Escape strings for 2nd level injection protection.
            $subx = mysqli_real_escape_string($database->conn,$subx);
            $suby = mysqli_real_escape_string($database->conn,$suby);
            $subz = mysqli_real_escape_string($database->conn,$subz);
            $subtype = mysqli_real_escape_string($database->conn,$subtype);
    
    
            /* Base error checking */
            $field = "baseType";  //Use field name for username
            if(!$subtype || strlen($subtype = trim($subtype)) == 0){
               $form->setError($field, "* Base-type not entered.");
            }
            else{
               /* Spruce up username, check length */
               $subtype = stripslashes($subtype);
               if(strlen($subtype) > 3){
                  $form->setError($field, "* Invalid Base-type.");
               }
               else if(strlen($subtype) < 1){
                  $form->setError($field, "* Invalid Base-type.");
               }
               /* Check if username is not alphanumeric */
               else if(!preg_match("/^[A-Za-z0-9_]+$/", $subtype)){
                  $form->setError($field, "* Invalid Base-type.");
               }
            }
            
            $field = "coordX";  //Use field name for username
            if(!$subx || strlen($subx = trim($subx)) == 0){
               $form->setError($field, "* Not entered.");
            }
            else{
               /* Spruce up username, check length */
               $subx = stripslashes($subx);
               if($subx > 2000000){
                  $form->setError($field, "* Too far.");
               }
               else if($subx < -2000000){
                  $form->setError($field, "* Too far.");
               }
               /* Check if username is not alphanumeric */
               else if(!preg_match("/^[A-Za-z0-9_]+$/", $subx)){
                  $form->setError($field, "* Not integer.");
               }
            }
            
            $field = "coordY";  //Use field name for username
            if(!$suby || strlen($suby = trim($suby)) == 0){
               $form->setError($field, "* Not integer.");
            }
            else{
               /* Spruce up username, check length */
               $suby = stripslashes($suby);
               if($suby > 2000000){
                  $form->setError($field, "* Too far.");
               }
               else if($suby < -2000000){
                  $form->setError($field, "* Too far.");
               }
               /* Check if username is not alphanumeric */
               else if(!preg_match("/^[A-Za-z0-9_]+$/", $suby)){
                  $form->setError($field, "* Not integer.");
               }
            }
            
            $field = "coordZ";  //Use field name for username
            if(!$subz || strlen($subz = trim($subz)) == 0){
               $form->setError($field, "* Not entered.");
            }
            else{
               /* Spruce up username, check length */
               $subz = stripslashes($subz);
               if($subz > 2000000){
                  $form->setError($field, "* Too far.");
               }
               else if($subz < -2000000){
                  $form->setError($field, "* Too far.");
               }
               /* Check if username is not alphanumeric */
               else if(!preg_match("/^[A-Za-z0-9_]+$/", $subz)){
                  $form->setError($field, "* Not integer.");
               }
            }
            
            
            /* Errors exist, have user correct them */
            if($form->num_errors > 0){
               return 1;  //Errors with form
            }
            /* No errors, add the new account to the */
            else{
                if($database->addNewBase($subuser, $subx, $suby, $subz, $subtype))  {
                    return 0;  //New base added succesfully
                }
                else {
                    return 2;  //Base add attempt failed
                }
            }
}
   
   
   function register($subuser, $subpass, $subpass2, $subemail, $subemail2, $displayName, $steamName, $skypeName,$referedBy) {
      global $database, $form, $mailer;  //The database, form and mailer object
      
        $subuser = mysql_real_escape_string($subuser);
        $subpass = mysql_real_escape_string($subpass);
        $subpass2 = mysql_real_escape_string($subpass2);
        $subemail = mysql_real_escape_string($subemail);
        $subemail2 = mysql_real_escape_string($subemail2);
        $displayName = mysql_real_escape_string($displayName);
        $steamName = mysql_real_escape_string($steamName);
        $skypeName = mysql_real_escape_string($skypeName);
        $referedBy = mysql_real_escape_string($referedBy);
              
        
       
      /* Username error checking */
      $field = "username";  //Use field name for username
      if(!$subuser || strlen($subuser = trim($subuser)) == 0){
         $form->setError($field, "* Username not entered.");
      }
      else{
         /* Spruce up username, check length */
         $subuser = stripslashes($subuser);
         if(strlen($subuser) < 4){
            $form->setError($field, "* Username below 5 characters.");
         }
         else if(strlen($subuser) > 30){
            $form->setError($field, "* Username above 30 characters.");
         }
         /* Check if username is not alphanumeric */
         else if(!preg_match("/^[A-Za-z0-9_]+$/", $subuser)){
            $form->setError($field, "* Username not alphanumeric.");
         }
         /* Check if username is reserved */
         else if(strcasecmp($subuser, GUEST_NAME) == 0){
            $form->setError($field, "* Username reserved word.");
         }
         /* Check if username is already in use */
         else if($database->usernameTaken($subuser)){
            $form->setError($field, "* Username already in use.");
         }
         /* Check if username is banned */
         /*
         else if($database->usernameBanned($subuser)){
            $form->setError($field, "* Username banned");
         }
         */
      }

      /* Password error checking */
      $field = "password";  //Use field name for password
      if(!$subpass){
         $form->setError($field, "* Password not entered");
      }
      else{
         /* Spruce up password and check length*/
         $subpass = stripslashes($subpass);
         $subpass2 = stripslashes($subpass2);
         if(strlen($subpass) < 4){
            $form->setError($field, "* Password too short");
         }
         if(strlen($subpass) > 25){
             $form->setError($field, "* Password too long.");
         }
         if($subpass != $subpass2){
            $form->setError("password2", "* Passwords didn't match.");
         }
      }
      
      /* Email error checking */
        $field = "email";  //Use field name for email
        
      if(!$subemail || strlen($subemail = trim($subemail)) == 0){
         $form->setError($field, "* Email not entered.");
      }
      else{
         /* Check if valid email address */
         $regex = "/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i";
         if(!preg_match($regex,$subemail)){
            $form->setError($field, "* Email invalid.");
         }
         
         if($subemail != $subemail2){
            $form->setError("email2", "* Emails did not match.");
         }
         $subemail = stripslashes($subemail);
      }

      /* Errors exist, have user correct them */
      if($form->num_errors > 0){
         return 1;  //Errors with form
      }
      /* No errors, add the new account to the */
      else{
        $code = $this->generateCode();
        if($database->addToCache("registered_users",1))  
            if($database->addNewUser($subuser, $subpass, $subemail, $displayName, $steamName, $skypeName,$code,$referedBy)) {
                $mailer->sendWelcome($subuser,$subemail,$code);
                $subpass = $database->nCrypt($subpass,$subuser);
          	$database->updateUserField($subuser, "password", $subpass);
            return 0;  //New user added succesfully
         }
         else{
            return 2;  //Registration attempt failed
         }
      }
   }
   
   function resetPassword($subuser, $subpass, $subpass2, $subemail) {
      global $database, $form, $mailer;  //The database, form and mailer object
      
        $subuser = mysql_real_escape_string($subuser);
        $subpass = mysql_real_escape_string($subpass);
        $subpass2 = mysql_real_escape_string($subpass2);
        $subemail = mysql_real_escape_string($subemail);
     
       
      /* Username error checking */
      $field = "username";  //Use field name for username
      if(!$subuser || strlen($subuser = trim($subuser)) == 0){
         $form->setError($field, "* Username not entered.");
      }
      else{
         /* Spruce up username, check length */
         $subuser = stripslashes($subuser);
         if(strlen($subuser) < 4){
            $form->setError($field, "* Username below 5 characters.");
         }
         else if(strlen($subuser) > 30){
            $form->setError($field, "* Username above 30 characters.");
         }
         /* Check if username is not alphanumeric */
         else if(!preg_match("/^[A-Za-z0-9_]+$/", $subuser)){
            $form->setError($field, "* Username not alphanumeric.");
         }
         /* Check if username is reserved */
         else if(strcasecmp($subuser, GUEST_NAME) == 0){
            $form->setError($field, "* Username reserved word.");
         }
         /* Check if username is banned */
         /*
         else if($database->usernameBanned($subuser)){
            $form->setError($field, "* Username banned");
         }
         */
      }

      /* Password error checking */
      $field = "password";  //Use field name for password
      if(!$subpass){
         $form->setError($field, "* Password not entered");
      }
      else{
         /* Spruce up password and check length*/
         $subpass = stripslashes($subpass);
         $subpass2 = stripslashes($subpass2);
         if(strlen($subpass) < 4){
            $form->setError($field, "* Password too short");
         }
         if(strlen($subpass) > 25){
             $form->setError($field, "* Password too long.");
         }
         if($subpass != $subpass2){
            $form->setError("password2", "* Passwords didn't match.");
         }
      }
      
      /* Email error checking */
        $field = "email";  //Use field name for email
        
      if(!$subemail || strlen($subemail = trim($subemail)) == 0){
         $form->setError($field, "* Email not entered.");
      }
      else{
         /* Check if valid email address */
         $regex = "/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i";
         if(!preg_match($regex,$subemail)){
            $form->setError($field, "* Email invalid.");
         }
         
         $userinfo = $database->getUserInfo($subuser);
         
         if($userinfo['email'] != $subemail){
            $form->setError($field, "* Email invalid.");
         }
         $subemail = stripslashes($subemail);
      }

      /* Errors exist, have user correct them */
      if($form->num_errors > 0){
         return 1;  //Errors with form
      }
      /* No errors, add the new account to the */
      else{
            $code = $this->generateCode();
            if($database->addResetInfo($subuser, $subpass, $code)) {
                $mailer->sendResetEmail($subuser,$subemail,$code);
            return 0;  //New user added succesfully
         }
         else{
            return 2;  //Registration attempt failed
         }
      }
   }
   
   
   function isAdmin(){
      return ($this->userlevel == ADMIN_LEVEL ||
              $this->username  == ADMIN_NAME || $this->userlevel >=7);
   }
   
   /**
    * generateRandID - Generates a string made up of randomized
    * letters (lower and upper case) and digits and returns
    * the md5 hash of it to be used as a userid.
    */
   function generateRandID(){
      return md5($this->generateRandStr(16));
   }
   
   /**
    * generateRandStr - Generates a string made up of randomized
    * letters (lower and upper case) and digits, the length
    * is a specified parameter.
    */
   function generateRandStr($length){
      $randstr = "";
      for($i=0; $i<$length; $i++){
         $randnum = mt_rand(0,61);
         if($randnum < 10){
            $randstr .= chr($randnum+48);
         }else if($randnum < 36){
            $randstr .= chr($randnum+55);
         }else{
            $randstr .= chr($randnum+61);
         }
      }
      return $randstr;
   }
   
   function generateCode() {
       $code = md5($this->generateRandStr(18));
       
       $result = mb_substr($code, 0, 6);
       
       return $result;
   }
};
$session = new Session;
$form = new Form;
?>