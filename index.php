<?php include("include/session.php"); global $form, $session, $database; $starttime = microtime(true); ?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Feed the Steve - Base Tool</title>

    <!-- Bootstrap -->
    <style>
    @import url('css/bootstrap.min.css');
    @import url('css/style.css');
    </style>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    
    <script type="text/javascript">
        $(function() {
            $(".tip").tooltip();
        })
    </script>
  </head>
  <body>
     
      <?php 
      //Include the navbar
      include_once 'module/nav.php';
      
      ?>
      
      <!--Whitelist List -->
    <div class="container">
        
        
        <?php 

        /*$alert = $database->latestAlert(); 
             
        echo '
        <div class="alert alert-danger">
            <span class="glyphicon glyphicon-exclamation-sign"></span> '.$alert['text'].' - '.$alert['username'].'
        </div>';*/
        
            
        //$result = $database->hasHQ($database->getUserID($session->username));
        //cho $result;
        //echo '<br>Username sent: '.$session->username.
                //'<br>ID found & sent: '.$database->getUserID($session->username);
        
        if(isset($_SESSION['alert']))
            echo '
        <div class="alert alert-danger">
            <span class="glyphicon glyphicon-exclamation-sign"></span> '.$_SESSION['alert'].'</div>';
        
        unset($_SESSION['alert']);
        
        if($session->logged_in)
            include_once 'module/invites.php';
        ?>
        
        
        
        <div class="row">
            <div class="col-md-12"><h2>Server Info</h2></div>
        </div>
       <?php echo "Members: ".$database->num_members; ?>
        <div class="row">
            <div class="col-md-6">
                <div class="well well-sm">
                    <h4>Welcome to Feed the Steve! </h4><p>This is a private white-listed server running the FTB(Monster) mod-pack. If you have been invited to the server by another player, please read the rules and follow them to the letter to keep the playing experience pleasant for everyone!</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="well well-sm">
                    <h4>Rules</h4>
                    <ul>
                        <li>Spawn is a safezone; No griefing/PvP/Stealing</li>
                        <li>Home-Bases can:</li>
                        <ul>
                            <li>only store items/power</li>
                            <li>not be griefed or raided</li>
                            <li>only contain vanilla blocks (No IC2/BC2 etc..)</li>
                            <ul>
                                <li>BetterStorage allowed</li>
                                <li>Power batteries / containers allowed</li>
                                <li>Aesthetic mods only (Micro Blocks)</li>
                                <li>Teleportation devices allowed (Be careful)</li>
                            </ul>
                            <li>only contain a maximum of 2 furnaces</li>
                            <li>not have mass production capabilities</li>
                            
                        </ul>
                        <li>No use of zans/reis minimap (Only use Map Writer)</li>
                        <li>No breaking a truce (Meeting up etc...)</li>
                        
                    </ul>
                </div>
            </div>
            
        </div>
            
        
        <div class="row">
            <div class="col-md-12"><h2>Player's Headquarters</h2></div>
        </div>
        
        <div class="row">
            <div class="col-md-8">
                <table class="table table-hover row">
                    <?php $database->displayHomeBases();  ?>
                </table>
            </div>
            <div class="col-md-4">
                <div class="well well-sm">
                    <h4>Submit a Base</h4>
                    
                    <form role="form" class="container-fluid" action="process.php" method="POST">
                        <h5>Coordinates</h5>
                        <div class="row">
                            <div class="col-xs-4">
                               <?php  if($form->num_errors > 0) {
                                echo '<input type="text" class="form-control" name="coordX" placeholder="X">';
                                 echo $form->error("coordX"); 
                                 }
                                else 
                                    echo '<input type="text" class="form-control" name="coordX" placeholder="X">';
                                ?>
                            </div>
                            
                            <div class="col-xs-4">
                                
                                <input type="text" class="form-control" name="coordY" placeholder="Y">
                                <?php echo $form->error("coordY"); ?>
                            </div>
                            <div class="col-xs-4">
                                
                                <input type="text" class="form-control" name="coordZ" placeholder="Z">
                                <?php echo $form->error("coordZ"); ?>
                            </div>
                        </div>
                        <h5>Base Type</h5>
                        <div class="row">
                            <?php echo $form->error("baseType"); ?>
                            <div class="col-md-12">
                                <select class="form-control" name="baseType">
                                    <option value="1">Headquarters</option>
                                    <option value="2">Outpost</option>
                                    <option value="3">Market</option>
                                </select>
                            </div>
                            
                        </div>
                        <br/>
                        <div class="row">
                            <input type="hidden" name="subNewBase" value="1"/>
                           <button type="submit" class="btn btn-primary col-xs-12">Submit Base</button>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
<?php
        $endtime = microtime(true);
        $duration = $endtime - $starttime;
      echo '<tr>Pageload took: </tr>'.$duration;
?>
      
  </body>
</html>